# wrapdb

This repository contains Meson wrap files that can be added to projects. I add
wrap files to this repository before they are available in the upstream WrapDB.

To use them, simply download the wrap file for the version of the project you
would like to use and place it in the 'subprojects' directory of your Meson
project. See the
[Meson Wrap Documentation](https://mesonbuild.com/Wrap-dependency-system-manual.html)
for more information.